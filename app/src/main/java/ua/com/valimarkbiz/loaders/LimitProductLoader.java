package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.LimitProductService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.SessionRequest;
import ua.com.valimarkbiz.models.product.ProductResponse;
import ua.com.valimarkbiz.models.product.RespList;

/**
 * Created by yurak on 02.05.2016.
 */
public class LimitProductLoader extends BaseLoader {

   // private final String apiKey;
    private String [] category;
    private final String limit = "3";
    private final String page = "1";

    public LimitProductLoader(Context context, String [] id) {
        super(context);
        this.category = id;
        //apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
       // Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        ArrayList<ProductResponse> productResponses = new ArrayList<>();
        for(int i = 0; i<category.length;i++) {
            Log.d("SESSION", PrefUtils.SESSION);
            LimitProductService service = ApiFactory.getLimitProductService();
            Call<ProductResponse> call = service.productResponce(PrefUtils.SESSION, category[i], limit, page);
            ProductResponse body = call.execute().body();
            productResponses.add(body);
            Log.d("BODY", body.getProducts().get(0).getImage());
        }
        RespList r = new RespList();
        r.setProductResponses(productResponses);
        return new BaseResponce()
                .setAnswer(r);
    }
}