package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.GetShipingService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 01.06.2016.
 */
public class GetShipinMethodsLoader extends BaseLoader
{

    public GetShipinMethodsLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        GetShipingService service = ApiFactory.getShipingService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<Sucsess> call = service.login(PrefUtils.SESSION);
        Sucsess body = call.execute().body();

        return new Response().setAnswer(body);
    }
}