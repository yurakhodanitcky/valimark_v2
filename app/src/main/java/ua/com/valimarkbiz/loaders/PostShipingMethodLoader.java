package ua.com.valimarkbiz.loaders;

import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.PostShipingService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.POST.ShippingMethod;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 20.06.2016.
 */
public class PostShipingMethodLoader extends BaseLoader {

    private ShippingMethod shippingMethod = new ShippingMethod();

    public PostShipingMethodLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {

        PostShipingService postShipingService = ApiFactory.postShipingService();
        Call<Sucsess> call = postShipingService.call(PrefUtils.SESSION, shippingMethod);
        Sucsess body = call.execute().body();

        return new Response().setAnswer(body);
    }
}