package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.GetPaymentService;
import ua.com.valimarkbiz.api.interfaces.PostConfirm;
import ua.com.valimarkbiz.api.interfaces.PostPaymentService;
import ua.com.valimarkbiz.api.interfaces.PutConfirm;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public class ConfirmLoader extends BaseLoader {
    //private final String apiKey;

    public ConfirmLoader (Context context) {
        super(context);
       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
        //Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        PostConfirm service = ApiFactory.postConfirm();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<Sucsess> call = service.postConfirm(PrefUtils.SESSION);
        Sucsess body = call.execute().body();

        PutConfirm putConfirm = ApiFactory.putConfirm();
        Call<Sucsess> call1 = putConfirm.putConfirm(PrefUtils.SESSION);
        Sucsess body1 = call1.execute().body();
        //Log.d("LOGIN_RESPONCE", call.execute().body().toString());

        return new Response().setAnswer(body);
    }
}
