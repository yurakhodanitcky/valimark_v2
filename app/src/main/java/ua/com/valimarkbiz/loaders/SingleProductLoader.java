package ua.com.valimarkbiz.loaders;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.SingleProductService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.product.SingleProduct;

import android.content.Context;
import android.util.Log;


public class SingleProductLoader  extends BaseLoader
{

	//private final String apiKey;
	private String id;

	public SingleProductLoader (Context context, String id) {
		super(context);
		this.id = id;
		//apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
		Log.d("APIKEY", PrefUtils.SESSION);
	}

	@Override
	protected Response apiCall() throws IOException {
		SingleProductService service = ApiFactory.getSingleProductService();
		Call<SingleProduct> call = service.productResponce(PrefUtils.SESSION, id);
		//Log.d("BODY", call.execute().body().toString());
		SingleProduct body = call.execute().body();

		return new BaseResponce()
				// .setRequestResult(RequestResult.SUCCESS)
				.setAnswer(body);
	}
	
}
