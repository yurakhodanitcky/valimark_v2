package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.CategoriesService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Categories;
import ua.com.valimarkbiz.models.Data;

public class CategoryLoader extends BaseLoader {

   // String apiKey;
    public CategoryLoader(Context context) {
        super(context);

       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
        //Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        CategoriesService service = ApiFactory.getCategoriesService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<Categories> call = service.categories(PrefUtils.SESSION);
        Categories body = call.execute().body();
        return new BaseResponce()
                .setAnswer(body);
    }
}