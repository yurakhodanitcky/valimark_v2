package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.GestPaymantService;
import ua.com.valimarkbiz.api.interfaces.GestShipingService;
import ua.com.valimarkbiz.api.interfaces.GetShipingService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.LoginRecponce;
import ua.com.valimarkbiz.models.POST.Gest;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public class GestPaymentLoader extends BaseLoader {

   //private final String apiKey;
    private Gest gest;
    public GestPaymentLoader (Context context, Gest gest) {
        super(context);
        this.gest = gest;
       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
       // Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        GestPaymantService service = ApiFactory.getGestPaymantService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<Sucsess> call = service.newPost(PrefUtils.SESSION, gest);
        Sucsess body = call.execute().body();

        GestShipingService service1 = ApiFactory.getGestShipingService();
        Call<Sucsess> call1 = service1.newPost(PrefUtils.SESSION, gest);

        //Log.d("LOGIN_RESPONCE", call.execute().body().toString());
        Sucsess body1 = call1.execute().body();
        return new Response().setAnswer(body);
    }
}
