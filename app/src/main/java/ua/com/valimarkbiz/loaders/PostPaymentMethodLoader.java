package ua.com.valimarkbiz.loaders;

import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.PostPaymentService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.POST.PaymentMethod;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 20.06.2016.
 */
public class PostPaymentMethodLoader extends BaseLoader {

    private PaymentMethod paymentMethod = new PaymentMethod();

    public PostPaymentMethodLoader(Context context) {
        super(context);

    }

    @Override
    protected Response apiCall() throws IOException {
        PostPaymentService postShipingService = ApiFactory.postPaymentService();
        Call<Sucsess> call = postShipingService.paymentMethod(PrefUtils.SESSION, paymentMethod);
        Sucsess body = call.execute().body();

        return new Response().setAnswer(body);
    }
}