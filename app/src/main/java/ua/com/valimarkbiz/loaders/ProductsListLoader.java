package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.ProductListService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.product.ProductResponse;


public class ProductsListLoader extends BaseLoader
{

   // private final String apiKey;
    private String id;

    public ProductsListLoader(Context context, String id) {
        super(context);
        this.id = id;
       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
       // Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        ProductListService service = ApiFactory.getProductsListService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<ProductResponse> call = service.productResponce(PrefUtils.SESSION, id);
        ProductResponse body = call.execute().body();
        return new Response().setAnswer(body);
    }
}