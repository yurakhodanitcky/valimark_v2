package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.AccountInfoService;
import ua.com.valimarkbiz.api.interfaces.LoginService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.LoginClass;
import ua.com.valimarkbiz.models.LoginRecponce;

/**
 * Created by yurak on 01.06.2016.
 */
public class AccountLoader extends BaseLoader
{
   // private final String apiKey;

    public AccountLoader(Context context) {
        super(context);
       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
        //Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        AccountInfoService service = ApiFactory.getAccountInfoService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<LoginRecponce> call = service.login(PrefUtils.SESSION);

        Log.d("LOGIN_RESPONCE", call.execute().body().toString());
        LoginRecponce body = call.execute().body();
        return new Response().setAnswer(body);
    }
}