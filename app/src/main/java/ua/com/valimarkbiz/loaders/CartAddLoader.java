package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.CartAddService;
import ua.com.valimarkbiz.api.interfaces.SingleProductService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.POST.CartAdd;
import ua.com.valimarkbiz.models.POST.CartAddSucsess;
import ua.com.valimarkbiz.models.product.SingleProduct;

/**
 * Created by yurak on 10.06.2016.
 */
public class CartAddLoader extends BaseLoader {
   // private final String apiKey;
    private String id;
    private HashMap<String, String> options;
    private CartAdd cartAdd;

    public CartAddLoader (Context context, String id, HashMap <String, String> options) {
        super(context);
        cartAdd = new CartAdd("1",id, options);
        //apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
        //Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        CartAddService service = ApiFactory.getCartAddService();
        Call<CartAddSucsess> call = service.cartAdd(PrefUtils.SESSION, cartAdd);
        //Log.d("BODY", call.execute().body().toString());
        CartAddSucsess body = call.execute().body();

        return new BaseResponce()
                // .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(body);
    }

}
