package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.GetPaymentService;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.POST.PaymentMethod;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public class GetPaymentMethodsLoader extends BaseLoader {

    public GetPaymentMethodsLoader(Context context) {
        super(context);

    }

    @Override
    protected Response apiCall() throws IOException {
        GetPaymentService service = ApiFactory.getPaymentService();
        Log.d("SESSION", PrefUtils.SESSION);
        Call<Sucsess> call = service.getPayment(PrefUtils.SESSION);
        Sucsess body = call.execute().body();
        return new Response().setAnswer(body);
    }
}
