package ua.com.valimarkbiz.loaders;

import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.SessionService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.models.SessionRequest;


public class SessionLoader extends BaseLoader {

    public SessionLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        SessionService service = ApiFactory.getSessionService();
        Call<SessionRequest> call = service.session();
        SessionRequest body = call.execute().body();
        return new BaseResponce()
                // .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(body);
    }
}