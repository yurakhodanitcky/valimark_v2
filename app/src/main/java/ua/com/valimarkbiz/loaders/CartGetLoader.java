package ua.com.valimarkbiz.loaders;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.interfaces.CartGetService;
import ua.com.valimarkbiz.api.responce.BaseResponce;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.getcart.GetCart;

/**
 * Created by yurak on 12.06.2016.
 */
public class CartGetLoader extends BaseLoader{
   // private final String apiKey;

    public CartGetLoader (Context context) {
        super(context);
       // apiKey = PrefUtils.getFromPrefs(getContext(), PrefUtils.APP_PREFERENCES_TOKEN, null); //Data.first(Data.class).getSession();
       // Log.d("APIKEY", apiKey);
    }

    @Override
    protected Response apiCall() throws IOException {
        CartGetService service = ApiFactory.getCartService();
        Call<GetCart> call = service.cartGet(PrefUtils.SESSION);
        GetCart body = call.execute().body();

        return new BaseResponce()
                // .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(body);
    }
}
