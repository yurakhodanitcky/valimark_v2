package ua.com.valimarkbiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ua.com.valimarkbiz.adapters.CartRecyclerAdapter;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.ConnectionDetector;
import ua.com.valimarkbiz.loaders.CartGetLoader;
import ua.com.valimarkbiz.models.getcart.CartProduct;
import ua.com.valimarkbiz.models.getcart.CartProducts;
import ua.com.valimarkbiz.models.getcart.GetCart;

public class CartActivity extends AppCompatActivity
	implements LoaderManager.LoaderCallbacks<Response>
{

	private CartRecyclerAdapter cra;

	private RecyclerView rView;

	private ArrayList<CartProduct> products;// = new ArrayList<Product>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cart_activity);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_cart);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_cart);
		fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view)
				{

					Snackbar.make(view, "В наличии", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
					CartActivity.this.startActivity(new Intent(CartActivity.this, GestCheckout.class));
				}
			});
		rView = (RecyclerView)findViewById(R.id.cart_recycler);
		RelativeLayout rl = (RelativeLayout)findViewById(R.id.noConnectionLayout);
		ConnectionDetector detector = new ConnectionDetector(this);
		if (detector.isConnected())
		{
			rl.setVisibility(View.GONE);
			getSupportLoaderManager().initLoader(0, Bundle.EMPTY, this);
		}
		else
		{
			rView.setVisibility(View.INVISIBLE);
		}


		Button refreshBtn = (Button)findViewById(R.id.refreshButton);
		refreshBtn.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1)
				{
					// TODO: Implement this method
					finish();
					startActivity(getIntent());
				}
			});
	}

	@Override
	public Loader<Response> onCreateLoader(int id, Bundle args) {
		return new CartGetLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<Response> loader, Response data) {
		GetCart cartProducts = data.getTypedAnswer();
		ArrayList<CartProduct> cartProduct = cartProducts.getCartProducts().getCartProducts();
		cra = new CartRecyclerAdapter(this, cartProduct);
		rView.setAdapter(cra);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		rView.setLayoutManager(layoutManager);
	}

	@Override
	public void onLoaderReset(Loader<Response> loader) {

	}

}
