package ua.com.valimarkbiz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ua.com.valimarkbiz.R;
import ua.com.valimarkbiz.SingleItemView;
import ua.com.valimarkbiz.helper.CircleTransform;
import ua.com.valimarkbiz.models.getcart.CartProduct;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.ViewHolder>
{

    private ArrayList<CartProduct> filteredData;
    private LayoutInflater mInflater;
    Context context;

    public CartRecyclerAdapter(Context context, ArrayList<CartProduct> data)
    {
        this.filteredData = data;
        this.context = context;
        mInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	public void setFilteredData(ArrayList<CartProduct> filteredData)
	{
		this.filteredData = filteredData;
	}

	public ArrayList<CartProduct> getFilteredData()
	{
		return filteredData;
	}



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
        View v = mInflater.inflate(R.layout.cart_card, parent, false);

        ViewHolder vh = new ViewHolder(v, this);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int p2)
	{
		Integer s = filteredData.get(p2).getQuantity();
		String price = filteredData.get(p2).getPrice();

		vh.mQuantity.setText(s.toString());
		if (vh.mQuantity.getText().toString().equals("1"))
		{
			vh.mMinus.setEnabled(false);
			vh.mMinus.setAlpha(125);
		}
		vh.mProduct = filteredData.get(p2);
        vh.mName.setText(Html.fromHtml(filteredData.get(p2).getName().toString()));
       
		vh.mPrice.setText(price.toString() + "₴");
        Picasso.with(context)
			.load("http://701822.valimsit.web.hosting-test.net/image/" + filteredData.get(p2).getThumb().replace("/home/valimsit/valimtest.com.ua/www/image/", ""))
			.transform(new CircleTransform())
			.fit().centerCrop()
			.into(vh.mImage);
      //  ArrayAdapter<String> spinnerArrayAdapterColors = new ArrayAdapter<String>(context,
	//																			  /*R.layout.spin_l*/android.R.layout.simple_spinner_dropdown_item, filteredData.get(p2).getColors());
      //  ArrayAdapter<String> spinnerArrayAdapterSizes = new ArrayAdapter<String>(context,
		//																		 /*R.layout.spin_l*/android.R.layout.simple_spinner_dropdown_item , filteredData.get(p2).getSizes());
      //  vh.mcolorSpinner.setAdapter(spinnerArrayAdapterColors);
       // vh.msizeSpinner.setAdapter(spinnerArrayAdapterSizes);

	}
    @Override
    public int getItemCount()
	{
        return filteredData.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder 
	implements View.OnClickListener
	{
		CartRecyclerAdapter adapter;
        private ImageButton mPlus;
        private ImageButton mMinus;
        private TextView mPrice;
        private TextView mName;
        private TextView mQuantity;
        private View mView;
        private ImageView mImage;
        private Spinner mcolorSpinner;
        private Spinner msizeSpinner;
		private CartProduct mProduct;
		private ImageButton mOverflow;
		private PopupMenu mPopupMenu;
        public ViewHolder(View itemView, final CartRecyclerAdapter adapter)
		{
            super(itemView);

			this.adapter = adapter;
            mView = itemView;
			mOverflow = (ImageButton)itemView.findViewById(R.id.cart_overflow);
            mPlus = (ImageButton)itemView.findViewById(R.id.plus);
            mMinus = (ImageButton)itemView.findViewById(R.id.minus);
            mPrice = (TextView)itemView.findViewById(R.id.cart_view_Price);
            mName = (TextView)itemView.findViewById(R.id.cart_view_Name);
            mQuantity = (TextView)itemView.findViewById(R.id.quantity);
            mImage = (ImageView)itemView.findViewById(R.id.cart_img);
            mcolorSpinner = (Spinner)itemView.findViewById(R.id.spinner);
            msizeSpinner = (Spinner)itemView.findViewById(R.id.spinner2);

            mPlus.setOnClickListener(this);
            mMinus.setOnClickListener(this);
            mView.setOnClickListener(this);

			mOverflow.setOnClickListener(this);

			mPopupMenu = new PopupMenu(mView.getContext(), mOverflow);
			final Menu menu = mPopupMenu.getMenu();

			mPopupMenu.getMenuInflater().inflate(R.menu.cart_menu, menu);
			mPopupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener(){

					@Override
					public boolean onMenuItemClick(MenuItem p1)
					{
						int id = p1.getItemId();
						boolean status = false;
						switch (id)
						{
							case R.id.action_remove:

								adapter.filteredData.remove(getAdapterPosition());
								adapter.notifyItemRemoved(getPosition());
								adapter.notifyDataSetChanged();
								status = true;
								break;
							case R.id.action_wishlist:
								Toast.makeText(mOverflow.getContext(), "wish", Toast.LENGTH_SHORT).show();
								status = true;
								break;
						}
						// TODO: Implement this method
						return status;
					}
				});
				
        }

        @Override
        public void onClick(View v)
		{

			Integer quantity = Integer.parseInt(mQuantity.getText().toString());
			//Integer price = mProduct.getIntegerPrice();
			Integer summ = Integer.valueOf(mPrice.getText().toString().replaceAll("\\D+", ""));
			if (v.getId() == mPlus.getId())
			{
				quantity++;
				//summ += price;
				mPrice.setText(summ.toString() + "₴");
				mQuantity.setText(quantity.toString());
				mMinus.setEnabled(true);
				mMinus.setAlpha(255);
			}
			else if (v.getId() == mMinus.getId())
			{
				quantity--;


				mPrice.setText(summ.toString() + "₴");
				mQuantity.setText(quantity.toString());

			}
			else if (v.getId() == mOverflow.getId())
			{
				mPopupMenu.show();
			}
			else
			{
				//Intent intent = new Intent(v.getContext(), SingleItemView.class);

				//intent.putExtra("name", mProduct.getName());
				// Pass all data country
				//intent.putExtra("price", mProduct.getPrice());
				// Pass all data population
				//intent.putExtra("descr", mProduct.getDescription());
				//intent.putExtra("map", categoriesList);
				//intent.putExtra("cat", cat);
				// Pass all data flag
				//intent.putExtra("img", "http://701822.valimsit.web.hosting-test.net/image/" + mProduct.getThumb());
				//intent.putExtra("url", "http://701822.valimsit.web.hosting-test.net/index.php?route=feed/web_api/product&id=" + mProduct.getId().toString() + "&key=android");
				// Start SingleItemView Class
				//v.getContext().startActivity(intent);
			}
        }
    }
}
