package ua.com.valimarkbiz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ua.com.valimarkbiz.R;
import ua.com.valimarkbiz.SingleItemView;
import ua.com.valimarkbiz.helper.CircleTransform;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.product.Product;


public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder>
{

	private ArrayList<Product> filteredData;
	private ArrayList<Product> cartProduct;
	private LayoutInflater mInflater;
	Context context;


	public SearchResultsAdapter(Context context, ArrayList<Product> data)
	{
		this.filteredData = data;

		this.context = context;
		this.mInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public void setFilter(ArrayList<Product> prod)
	{
        filteredData = new ArrayList<>();
        filteredData.addAll(prod);
        notifyDataSetChanged();
    }

	public void setFilteredData(ArrayList<Product> filteredData)
	{
		this.filteredData = filteredData;
	}

	public ArrayList<Product> getFilteredData()
	{
		return filteredData;
	}


	@Override
	public SearchResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup p1, int p2)
	{
		View v = mInflater.inflate(R.layout.product_view, p1, false);

		ViewHolder vh = new ViewHolder(v, filteredData.get(p2));

        return vh;
	}

	@Override
	public void onBindViewHolder(final SearchResultsAdapter.ViewHolder vh, int p2)
	{
		if (filteredData.get(p2).getQuantity() == 0)
		{
            vh.mQuantity.setText(filteredData.get(p2).getQuantity().toString());
            vh.mImStock.setImageResource(R.drawable.out_stock);
			vh.mIBtn.setEnabled(false);
			vh.mIBtn.setAlpha(155);
		}
		else
		{
			vh.mQuantity.setText(filteredData.get(p2).getQuantity().toString());
			vh.mImStock.setImageResource(R.drawable.in_stock);
						vh.mIBtn.setEnabled(true);
			vh.mIBtn.setAlpha(255);
			vh.mIBtn.setImageResource(R.drawable.ic_add_shopping_cart_white_24dp);
		}

		vh.mName.setText(Html.fromHtml(filteredData.get(p2).getName().toString()));
		vh.mPrice.setText(filteredData.get(p2).getPriceFormated().replaceAll("\\D+", "") + " ₴");

		Log.d("DEBUG",filteredData.get(p2).getImage());

		vh.product = filteredData.get(p2);
		Picasso.with(context)
			.load(filteredData.get(p2).getImage())
			.transform(new CircleTransform())
			.fit().centerCrop()
			.into(vh.mImage);

		/*Picasso.with(context)
		 .load("http://701822.valimsit.web.hosting-test.net/image/" + filteredData.get(p2).getImageUrl())
		 .fit().centerCrop()
		 .into(vh.mImage, new Callback.EmptyCallback() {

		 //When the image is loaded, palette will take the color and apply it to the other imageView

		 @Override public void onSuccess()
		 { 
		 final Bitmap bitmap = ((BitmapDrawable) vh.mImage.getDrawable()).getBitmap();// Ew! 
		 Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() { 
		 public void onGenerated(Palette palette)
		 { 

		 if (palette != null)
		 { 

		 Palette.Swatch vibrantSwatch = palette.getVibrantSwatch(); 

		 if (vibrantSwatch != null)
		 { 
		 vh.toolbar.setBackgroundColor(vibrantSwatch.getRgb()); 
		 //vh.mName.setTextColor(vibrantSwatch.getTitleTextColor()); 
		 //vh.mPrice.setTextColor(vibrantSwatch.getTitleTextColor());
		 } 
		 } 
		 } 
		 }); 
		 } 
		 });*/
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return filteredData.size();
	}

	public Product getItem(int p)
	{
		return filteredData.get(p);
	}

	static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
        // each data item is just a string in this case
        public TextView mName;
		public TextView mPrice;
		public TextView mQuantity;
		public ImageView mImage;
		public ImageButton mIBtn;
		public ImageButton mWish;
		public ImageView mImStock;
		public View view;

		private boolean isWishExist = false;
		private Product product;

		public ViewHolder(View v, Product p)
		{
			super(v);

			mName = (TextView)v.findViewById(R.id.product_view_Name);
			mPrice = (TextView)v.findViewById(R.id.product_view_Price);
			mImage = (ImageView)v.findViewById(R.id.productviewImageView1);
			mQuantity = (TextView)v.findViewById(R.id.product_view_qantity);
			mIBtn = (ImageButton)v.findViewById(R.id.product_viewImageButton);
			view = v;
			mImStock = (ImageView)v.findViewById(R.id.product_viewImageView);
			mWish = (ImageButton)v.findViewById(R.id.product_viewImageButtonWish);
			view.setOnClickListener(this);
			mIBtn.setOnClickListener(this);
			mWish.setOnClickListener(this);
			product = p;
			if (isWishExist)
			{
				mWish.setImageResource(R.drawable.ic_favorite_white_24dp);
			}
			else
			{
				mWish.setImageResource(R.drawable.ic_favorite_border_white_24dp);
			}
		}
		//boolean isInWish = false;

		@Override
		public void onClick(View p1)
		{
			if (p1.getId() == mIBtn.getId())
			{
				int y = Integer.valueOf(product.getItemId());
				mIBtn.setImageResource(R.drawable.ic_shopping_cart_white_24dp);
			}
			else if (p1.getId() == mWish.getId()) {
				if (isWishExist)
					mWish.setImageResource(R.drawable.ic_favorite_white_24dp);
				else
					mWish.setImageResource(R.drawable.ic_favorite_border_white_24dp);
			}
			else
			{
				Intent intent = new Intent(p1.getContext(), SingleItemView.class);

				intent.putExtra("ID", product.getItemId());
				p1.getContext().startActivity(intent);

				Toast.makeText(p1.getContext(), "view", Toast.LENGTH_SHORT).show();
			}
		}
    }
}
 
