package ua.com.valimarkbiz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ua.com.valimarkbiz.CategoryActivity;
import ua.com.valimarkbiz.R;
import ua.com.valimarkbiz.helper.CircleTransform;
import ua.com.valimarkbiz.models.Categories;
import ua.com.valimarkbiz.models.product.ProductResponse;

/**
 * Created by yurak on 02.05.2016.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>
{

    private ArrayList<Categories.Category> productResponses;
    private LayoutInflater mInflater;
    private Context context;
    public CategoriesAdapter(Context context, ArrayList<Categories.Category> data)
    {
        this.productResponses = data;
        this.context = context;
        this.mInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public CategoriesHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
        View view = mInflater.inflate(R.layout.content_main, parent, false);

        CategoriesHolder vh = new CategoriesHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(CategoriesHolder holder, int position)
	{
		// String[] images = new String[3];
		// ProductResponse productResponse = productResponses.get(position);
		/* for (int i = 0; i < productResponse.getProducts().size(); i++){
		 images[i]= "http://701822.valimsit.web.hosting-test.net/image/" + productResponse.getProducts().get(i).getImage();
		 Log.d("IMAGE",images[i]);
		 }*/
		/* Picasso.with(context)
		 .load(images[0])
		 .transform(new CircleTransform())
		 .fit().centerCrop()
		 .into(holder.image1);

		 Picasso.with(context)
		 .load(images[1])
		 .transform(new CircleTransform())
		 .fit().centerCrop()
		 .into(holder.image2);

		 Picasso.with(context)
		 .load(images[2])
		 .transform(new CircleTransform())
		 .fit().centerCrop()
		 .into(holder.image3);*/

        holder.categoryName.setText(productResponses.get(position).getName());
        holder.categoryId = productResponses.get(position).getCategoryId();
    }

    @Override
    public int getItemCount()
	{
        return productResponses.size();
    }

    static class CategoriesHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public TextView categoryName;
		//  public ImageView image1;
		// public ImageView image2;
		// public ImageView image3;
        public String categoryId;

        public CategoriesHolder(View itemView)
		{
            super(itemView);
            itemView.setOnClickListener(this);
            categoryName = (TextView)itemView.findViewById(R.id.categoryName);
			//  image1 = (ImageView)itemView.findViewById(R.id.imageView4);
			// image2 = (ImageView)itemView.findViewById(R.id.imageView2);
			// image3 = (ImageView)itemView.findViewById(R.id.imageView3);

        }

        @Override
        public void onClick(View v)
		{
			Intent intent = new Intent(v.getContext(), CategoryActivity.class);
			intent.putExtra("ID", categoryId);
            v.getContext().startActivity(intent);
            //CategoryActivity_.intent(v.getContext()).extra("ID", categoryId).start();
			Toast.makeText(v.getContext(), categoryId, Toast.LENGTH_LONG).show();
        }
    }
}
