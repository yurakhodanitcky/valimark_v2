package ua.com.valimarkbiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import ua.com.valimarkbiz.adapters.SearchResultsAdapter;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.ConnectionDetector;
import ua.com.valimarkbiz.loaders.ProductsListLoader;
import ua.com.valimarkbiz.models.product.Product;
import ua.com.valimarkbiz.models.product.ProductResponse;
import android.os.*;

/**
 * Created by yurak on 18.05.2016.
 */

public class CategoryActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LoaderManager.LoaderCallbacks<Response> {

    Toolbar toolbar;

    NavigationView navigationView;

   // @ViewById(R.id.contentMainRecyclerView)
    RecyclerView recyclerView;

   // @ViewById(R.id.drawer_cat)
    DrawerLayout drawer;

   // @ViewById(R.id.noConnectionLayout)
    RelativeLayout relativeLayout;

    private String categoryId;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.category_activity);
		
		toolbar = (Toolbar)findViewById(R.id.toolbar_cat);
        navigationView = (NavigationView)findViewById(R.id.nav_view_cat);
		recyclerView = (RecyclerView)findViewById(R.id.contentMainRecyclerView);
		drawer = (DrawerLayout)findViewById(R.id.drawer_cat);
		relativeLayout = (RelativeLayout)findViewById(R.id.noConnectionLayout);
		
		
		Intent intent = getIntent();
        categoryId = intent.getStringExtra("ID");
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        ConnectionDetector detector = new ConnectionDetector(this);

        if (detector.isConnected()) {
            relativeLayout.setVisibility(View.GONE);
            Bundle bundle = new Bundle();
            bundle.putString("ID", categoryId);
            getSupportLoaderManager().initLoader(0, bundle, this);
        }
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
       return new ProductsListLoader(this, args.getString("ID"));
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        ProductResponse productResponse = data.getTypedAnswer();
        ArrayList<Product> products = productResponse.getProducts();
        SearchResultsAdapter searchResultsAdapter = new SearchResultsAdapter(this, products);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(searchResultsAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
