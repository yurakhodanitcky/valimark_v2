package ua.com.valimarkbiz;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ua.com.valimarkbiz.adapters.CustomPagerAdapter;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.ConnectionDetector;
import ua.com.valimarkbiz.loaders.CartAddLoader;
import ua.com.valimarkbiz.loaders.CartGetLoader;
import ua.com.valimarkbiz.loaders.SingleProductLoader;
import ua.com.valimarkbiz.models.POST.CartAdd;
import ua.com.valimarkbiz.models.product.Attribute;
import ua.com.valimarkbiz.models.product.AttributeGroup;
import ua.com.valimarkbiz.models.product.OptionValue;
import ua.com.valimarkbiz.models.product.Options;
import ua.com.valimarkbiz.models.product.Product;
import ua.com.valimarkbiz.models.product.SingleProduct;
import ua.com.valimarkbiz.widget.CirclePageIndicator;


public class SingleItemView extends AppCompatActivity
implements LoaderManager.LoaderCallbacks<Response>, View.OnClickListener {

	private TableLayout tab;
	private Spinner spS;
	private Spinner spC;
    private ArrayList<String> urls = new ArrayList<String>();
    private String url;
    private TextView tPrice;
    private CirclePageIndicator mViewPagerIndicator;
	private ViewPager mViewPager;
    private RelativeLayout relativeLayoutBottom;
    private ImageButton imageButton;
    private HashMap<String, String> option = new HashMap<>();
    private Product product;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_activity);

		tPrice = (TextView) findViewById(R.id.singleproductTextView2);
		tab = (TableLayout) findViewById(R.id.single_productTableLayout);
		tab.setStretchAllColumns(true);
		tab.setShrinkAllColumns(true);
		spS = (Spinner) findViewById(R.id.singleproductSpinner2);
		spC = (Spinner) findViewById(R.id.singleproductSpinner1);
        imageButton = (ImageButton)findViewById(R.id.product_viewImageButton);
        imageButton.setOnClickListener(this);
        relativeLayoutBottom = (RelativeLayout)findViewById(R.id.toolbar_p_bottom);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_p);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		android.support.v4.widget.NestedScrollView scroll = (android.support.v4.widget.NestedScrollView) findViewById(R.id.productScroll);
		ImageButton imbtn = (ImageButton) findViewById(R.id.product_viewImageButton);


		Intent i = getIntent();
		url = i.getStringExtra("ID");
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.noConnectionLayout);
		ConnectionDetector detector = new ConnectionDetector(this);
		if (detector.isConnected()) {
			rl.setVisibility(View.GONE);
			Bundle bundle = new Bundle();
			bundle.putString("ID", url);
			getSupportLoaderManager().initLoader(0, bundle, this);
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPagerIndicator = (CirclePageIndicator)
					findViewById(R.id.viewpagerindicator);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

        if(id == R.id.action_settings){
           // getSupportLoaderManager().initLoader(3, Bundle.EMPTY, this);
            this.startActivity(new Intent(this, CartActivity.class));
        }
		return true;//super.onOptionsItemSelected(item);
	}

	@Override
	public Loader<Response> onCreateLoader(int p1, Bundle p2) {
        Loader<Response> mLoader = null;
        if (p1 == 0) {
            String id = p2.getString("ID");
            mLoader = new SingleProductLoader(SingleItemView.this, id);
        }else if(p1 == 1){
            HashMap<String, String> opt = (HashMap<String, String>) p2.getSerializable("MAP");
            mLoader = new CartAddLoader(this, p2.getString("ID"), opt);
        }else if(p1 == 3){
            mLoader = new CartGetLoader(this);
        }
        return mLoader;
    }

	@Override
	public void onLoadFinished(Loader<Response> loader, Response data) {
        if(loader.getId() == 1){

        }else if(loader.getId() == 0) {
            SingleProduct singleProduct = data.getTypedAnswer();
            product = singleProduct.getProduct();
            Log.d("SINGLE_PRODUCT", product.getName());
            getSupportActionBar().setTitle(product.getName());

            tPrice.setText(product.getPriceFormated().replaceAll("\\D+", "") + " ₴");
            final ArrayList<Options> options = product.getOptions();
            ArrayList<AttributeGroup> attributeGroups = product.getAttributeGroups();
            int i = 0;
            for (AttributeGroup a : attributeGroups) {
                TableRow row = new TableRow(SingleItemView.this);

                row.setLayoutParams(new LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                TextView v = new TextView(SingleItemView.this);
                TextView v1 = new TextView(SingleItemView.this);
                String gName = a.getName();
                v.setText(gName);
                String aName = "";
                ArrayList<Attribute> b = a.getAttribute();
                for (int y = 0; y < b.size(); y++) {

                    if (y + 1 == b.size()) {
                        aName += b.get(y).getName();
                    } else {
                        aName += b.get(y).getName() + "\n";
                    }
                }
                v1.setText(aName);
                v.setTextSize(19);
                v1.setTextSize(19);
                v.setPadding(10, 10, 10, 10);
                v1.setPadding(10, 10, 10, 10);
                v.setGravity(Gravity.START);
                v1.setGravity(Gravity.START);
                v.setTypeface(null, Typeface.BOLD);
                v1.setTypeface(null, Typeface.ITALIC);

                if (i % 2 == 0) {
                    row.setBackgroundResource(R.color.lightBackground);
                } else {
                    row.setBackgroundResource(R.color.colorPrimaryLight);
                }
                i++;
                row.addView(v);
                row.addView(v1);
                tab.addView(row);
                aName = "";
            }

            urls = product.getImages();
            CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(SingleItemView.this, urls);
            mViewPager.setAdapter(mCustomPagerAdapter);

            mViewPagerIndicator.setViewPager(mViewPager);
            mViewPagerIndicator.setVisibility(View.VISIBLE);
            if (product.getQuantity() > 0) {
                relativeLayoutBottom.setVisibility(View.VISIBLE);
                if (options.size() > 0) {
                    final ArrayList<OptionValue> oValue = options.get(0).getOptionValue();
                    String[] opt_1 = new String[oValue.size()];
                    for (int y = 0; y < oValue.size(); y++) {
                        opt_1[y] = oValue.get(y).getName().toUpperCase();
                    }
                    final ArrayList<OptionValue> oValue1 = options.get(1).getOptionValue();
                    String[] opt_2 = new String[oValue1.size()];
                    for (int y = 0; y < oValue1.size(); y++) {
                        opt_2[y] = oValue1.get(y).getName().toUpperCase();
                    }


                    ArrayAdapter<String> spinnerArrayAdapterS = new ArrayAdapter<String>(SingleItemView.this,
                            R.layout.spin_l, opt_1);


                    spinnerArrayAdapterS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spS.setAdapter(spinnerArrayAdapterS);
                    spS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String productOptionId = options.get(0).getProductOptionId();
                            String oValueId_1 = oValue.get(position).getProductOptionValueId();
                            option.put(productOptionId, oValueId_1);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(SingleItemView.this,
                            R.layout.spin_l, opt_2);


                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spC.setAdapter(spinnerArrayAdapter);
                    spC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String productOptionId = options.get(1).getProductOptionId();
                            String oValueId_2 = oValue1.get(position).getProductOptionValueId();
                            option.put(productOptionId, oValueId_2);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    spC.setVisibility(View.GONE);
                    spS.setVisibility(View.GONE);
                }
            } else {
                relativeLayoutBottom.setVisibility(View.INVISIBLE);
            }
        }
	}


	@Override
	public void onLoaderReset(Loader<Response> p1) {
		// TODO: Implement this method
	}

    @Override
    public void onClick(View v) {
        if (v.getId() == imageButton.getId()) {
            CartAdd cartAdd = new CartAdd("1", product.getItemId(), option);
            Bundle bundle = new Bundle();
            bundle.putString("ID", product.getItemId());
            bundle.putSerializable("MAP", option);
            if(getSupportLoaderManager().getLoader(1) == null) {
                getSupportLoaderManager().initLoader(1, bundle, this);
            }else{
                getSupportLoaderManager().restartLoader(1, bundle, this);
            }
        }
    }
}