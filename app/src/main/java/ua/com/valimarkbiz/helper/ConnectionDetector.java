package ua.com.valimarkbiz.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.*;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectionDetector
{

	private Context context;
	public ConnectionDetector(Context context)
	{
		this.context = context;
	}

	public Boolean isConnected()
	{
		CheckConnection cc = new CheckConnection(context);

		cc.execute();

		try
		{
			return cc.get();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (ExecutionException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private class CheckConnection extends AsyncTask<Void, Void, Boolean>
	{

		private Context context;

		public CheckConnection(Context context)
		{
			this.context = context;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Void... args)
		{
			boolean b = false;

			ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			if (activeNetwork != null && activeNetwork.isConnected())
			{
				
				try
				{
					OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
					OkHttpClient client = builder.connectTimeout(3000, TimeUnit.MILLISECONDS)
							.readTimeout(3000, TimeUnit.MILLISECONDS)
							.build();
					//client.connectTimeoutMillis() = 3000;
					//client.setReadTimeout(3000, TimeUnit.MILLISECONDS);
					URL url = new URL("http://clients3.google.com/generate_204");
					Request request = new Request.Builder()
						.url(url)
						.build();
					Response responses = null;
					responses = client.newCall(request).execute();

					if (responses.code() == 204)
					{
						//return true;
						b = true;
					}
					else
					{
						//return false;
						b = false;
					}
				}
				catch (IOException e)
				{

					b = false;
					Log.i("warning", "Error checking internet connection", e);
					//return false;
				}
			}
			return b;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			// TODO: Implement this method
			super.onPostExecute(result);
		}
	}
}
