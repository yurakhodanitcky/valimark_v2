
package ua.com.valimarkbiz.models.product;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.orm.dsl.Ignore;


public class Product{ //extends SugarRecord{

    @SerializedName("id")
    @Expose
    private String itemId;
    
    @SerializedName("name")
    @Expose
    private String name;
    
    @SerializedName("model")
    @Expose
	@Ignore
    private String model;
	
    @SerializedName("image")
    @Expose
	@Ignore
    private String image;
	
    @SerializedName("images")
    @Expose
	@Ignore
    private ArrayList<String> images = new ArrayList<String>();
	
    @SerializedName("price")
    @Expose
	@Ignore
    private String price;
	
    @SerializedName("price_formated")
    @Expose
	@Ignore
    private String priceFormated;
    
    @SerializedName("attribute_groups")
    @Expose
	@Ignore
    private ArrayList<AttributeGroup> attributeGroups = new ArrayList<AttributeGroup>();
    
    @SerializedName("options")
    @Expose
	@Ignore
    private ArrayList<Options> options = new ArrayList<Options>();
    
    @SerializedName("status")
    @Expose
	@Ignore
    private String status;
    
    @SerializedName("category")
    @Expose
	@Ignore
    private ArrayList<Category> category = new ArrayList<Category>();
	
    @SerializedName("quantity")
    @Expose
	@Ignore
    private Integer quantity;
    
	
	public Product(){}

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceFormated() {
        return priceFormated;
    }

    public void setPriceFormated(String priceFormated) {
        this.priceFormated = priceFormated;
    }

    public ArrayList<AttributeGroup> getAttributeGroups() {
        return attributeGroups;
    }

    public void setAttributeGroups(ArrayList<AttributeGroup> attributeGroups) {
        this.attributeGroups = attributeGroups;
    }

    public ArrayList<Options> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Options> options) {
        this.options = options;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Category> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<Category> category) {
        this.category = category;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
