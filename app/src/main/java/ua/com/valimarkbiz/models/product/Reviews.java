
package ua.com.valimarkbiz.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reviews {

    @SerializedName("review_total")
    @Expose
    private String reviewTotal;

    /**
     * 
     * @return
     *     The reviewTotal
     */
    public String getReviewTotal() {
        return reviewTotal;
    }

    /**
     * 
     * @param reviewTotal
     *     The review_total
     */
    public void setReviewTotal(String reviewTotal) {
        this.reviewTotal = reviewTotal;
    }

}
