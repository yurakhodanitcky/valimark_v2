
package ua.com.valimarkbiz.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleProduct {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Product product;

    /**
     * 
     * @return
     *     The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The data
     */
    public Product getProduct() {
        return product;
    }

    /**
     * 
     * @param product
     *     The data
     */
    public void setProduct(Product product) {
        this.product = product;
    }

}
