package ua.com.valimarkbiz.models.product;

import java.util.ArrayList;

/**
 * Created by yurak on 02.05.2016.
 */
public class RespList {
    private ArrayList<ProductResponse> productResponses;

    public ArrayList<ProductResponse> getProductResponses() {
        return productResponses;
    }

    public void setProductResponses(ArrayList<ProductResponse> productResponses) {
        this.productResponses = productResponses;
    }
}
