package ua.com.valimarkbiz.models.POST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yurak on 19.06.2016.
 */
public class Sucsess {
    @SerializedName("success")
    @Expose
    private Boolean success;

    /**
     * @return The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
