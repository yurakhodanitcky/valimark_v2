
package ua.com.valimarkbiz.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OptionValue {

    @SerializedName("product_option_value_id")
    @Expose
    private String productOptionValueId;
    @SerializedName("option_value_id")
    @Expose
    private String optionValueId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Boolean price;
    @SerializedName("price_prefix")
    @Expose
    private String pricePrefix;

    /**
     * 
     * @return
     *     The productOptionValueId
     */
    public String getProductOptionValueId() {
        return productOptionValueId;
    }

    /**
     * 
     * @param productOptionValueId
     *     The product_option_value_id
     */
    public void setProductOptionValueId(String productOptionValueId) {
        this.productOptionValueId = productOptionValueId;
    }

    /**
     * 
     * @return
     *     The optionValueId
     */
    public String getOptionValueId() {
        return optionValueId;
    }

    /**
     * 
     * @param optionValueId
     *     The option_value_id
     */
    public void setOptionValueId(String optionValueId) {
        this.optionValueId = optionValueId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The price
     */
    public Boolean getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(Boolean price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The pricePrefix
     */
    public String getPricePrefix() {
        return pricePrefix;
    }

    /**
     * 
     * @param pricePrefix
     *     The price_prefix
     */
    public void setPricePrefix(String pricePrefix) {
        this.pricePrefix = pricePrefix;
    }

}
