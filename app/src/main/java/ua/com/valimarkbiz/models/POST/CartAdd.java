package ua.com.valimarkbiz.models.POST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yurak on 10.06.2016.
 */
public class CartAdd {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("option")
    @Expose
    private HashMap<String, String> option;

    public CartAdd(String quantity, String productId, HashMap<String, String> option) {
        this.quantity = quantity;
        this.productId = productId;
        this.option = option;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public HashMap<String, String> getOption() {
        return option;
    }

    public void setOption(HashMap<String, String> option) {
        this.option = option;
    }
}
