
package ua.com.valimarkbiz.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Options {

    @SerializedName("product_option_id")
    @Expose
    private String productOptionId;
    @SerializedName("option_id")
    @Expose
    private String optionId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("option_value")
    @Expose
    private ArrayList<OptionValue> optionValue = new ArrayList<OptionValue>();
    @SerializedName("required")
    @Expose
    private String required;

    /**
     * 
     * @return
     *     The productOptionId
     */
    public String getProductOptionId() {
        return productOptionId;
    }

    /**
     * 
     * @param productOptionId
     *     The product_option_id
     */
    public void setProductOptionId(String productOptionId) {
        this.productOptionId = productOptionId;
    }

    /**
     * 
     * @return
     *     The optionId
     */
    public String getOptionId() {
        return optionId;
    }

    /**
     * 
     * @param optionId
     *     The option_id
     */
    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The optionValue
     */
    public ArrayList<OptionValue> getOptionValue() {
        return optionValue;
    }

    /**
     * 
     * @param optionValue
     *     The option_value
     */
    public void setOptionValue(ArrayList<OptionValue> optionValue) {
        this.optionValue = optionValue;
    }

    /**
     * 
     * @return
     *     The required
     */
    public String getRequired() {
        return required;
    }

    /**
     * 
     * @param required
     *     The required
     */
    public void setRequired(String required) {
        this.required = required;
    }

}
