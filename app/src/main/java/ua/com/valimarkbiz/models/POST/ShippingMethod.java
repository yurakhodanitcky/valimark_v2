package ua.com.valimarkbiz.models.POST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yurak on 19.06.2016.
 */
public class ShippingMethod {

    @SerializedName("shipping_method")
    @Expose
    private String shippingMethod = "free.free";
   /* @SerializedName("comment")
    @Expose
    private final String comment = "";

    /**
     * @return The shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @return The comment
     */
   /* public String getComment() {
        return comment;
    }*/

}