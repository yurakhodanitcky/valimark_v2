
package ua.com.valimarkbiz.models.getcart;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartProducts {

    @SerializedName("error_warning")
    @Expose
    private String errorWarning;
    @SerializedName("attention")
    @Expose
    private String attention;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("products")
    @Expose
    private ArrayList<CartProduct> cartProducts = new ArrayList<CartProduct>();
    @SerializedName("vouchers")
    @Expose
    private ArrayList<Object> vouchers = new ArrayList<Object>();
    @SerializedName("coupon_status")
    @Expose
    private Object couponStatus;
    @SerializedName("coupon")
    @Expose
    private String coupon;
    @SerializedName("voucher_status")
    @Expose
    private Object voucherStatus;
    @SerializedName("voucher")
    @Expose
    private String voucher;
    @SerializedName("reward_status")
    @Expose
    private Boolean rewardStatus;
    @SerializedName("reward")
    @Expose
    private String reward;
    @SerializedName("totals")
    @Expose
    private ArrayList<Total> totals = new ArrayList<Total>();

    /**
     * 
     * @return
     *     The errorWarning
     */
    public String getErrorWarning() {
        return errorWarning;
    }

    /**
     * 
     * @param errorWarning
     *     The error_warning
     */
    public void setErrorWarning(String errorWarning) {
        this.errorWarning = errorWarning;
    }

    /**
     * 
     * @return
     *     The attention
     */
    public String getAttention() {
        return attention;
    }

    /**
     * 
     * @param attention
     *     The attention
     */
    public void setAttention(String attention) {
        this.attention = attention;
    }

    /**
     * 
     * @return
     *     The weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * 
     * @param weight
     *     The weight
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * 
     * @return
     *     The products
     */
    public ArrayList<CartProduct> getCartProducts() {
        return cartProducts;
    }

    /**
     * 
     * @param cartProducts
     *     The products
     */
    public void setCartProducts(ArrayList<CartProduct> cartProducts) {
        this.cartProducts = cartProducts;
    }

    /**
     * 
     * @return
     *     The vouchers
     */
    public ArrayList<Object> getVouchers() {
        return vouchers;
    }

    /**
     * 
     * @param vouchers
     *     The vouchers
     */
    public void setVouchers(ArrayList<Object> vouchers) {
        this.vouchers = vouchers;
    }

    /**
     * 
     * @return
     *     The couponStatus
     */
    public Object getCouponStatus() {
        return couponStatus;
    }

    /**
     * 
     * @param couponStatus
     *     The coupon_status
     */
    public void setCouponStatus(Object couponStatus) {
        this.couponStatus = couponStatus;
    }

    /**
     * 
     * @return
     *     The coupon
     */
    public String getCoupon() {
        return coupon;
    }

    /**
     * 
     * @param coupon
     *     The coupon
     */
    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    /**
     * 
     * @return
     *     The voucherStatus
     */
    public Object getVoucherStatus() {
        return voucherStatus;
    }

    /**
     * 
     * @param voucherStatus
     *     The voucher_status
     */
    public void setVoucherStatus(Object voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    /**
     * 
     * @return
     *     The voucher
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * 
     * @param voucher
     *     The voucher
     */
    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    /**
     * 
     * @return
     *     The rewardStatus
     */
    public Boolean getRewardStatus() {
        return rewardStatus;
    }

    /**
     * 
     * @param rewardStatus
     *     The reward_status
     */
    public void setRewardStatus(Boolean rewardStatus) {
        this.rewardStatus = rewardStatus;
    }

    /**
     * 
     * @return
     *     The reward
     */
    public String getReward() {
        return reward;
    }

    /**
     * 
     * @param reward
     *     The reward
     */
    public void setReward(String reward) {
        this.reward = reward;
    }

    /**
     * 
     * @return
     *     The totals
     */
    public ArrayList<Total> getTotals() {
        return totals;
    }

    /**
     * 
     * @param totals
     *     The totals
     */
    public void setTotals(ArrayList<Total> totals) {
        this.totals = totals;
    }

}
