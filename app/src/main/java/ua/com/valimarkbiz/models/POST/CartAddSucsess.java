package ua.com.valimarkbiz.models.POST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yurak on 10.06.2016.
 */
public class CartAddSucsess {


        @SerializedName("success")
        @Expose
        public String success;
        @SerializedName("product")
        @Expose
        public Product product;
        @SerializedName("total")
        @Expose
        public String total;


    public class Product {

        @SerializedName("product_id")
        @Expose
        public String productId;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("quantity")
        @Expose
        public String quantity;

    }
}
