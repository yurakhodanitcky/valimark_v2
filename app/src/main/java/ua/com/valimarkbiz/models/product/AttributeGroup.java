
package ua.com.valimarkbiz.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class AttributeGroup {

    @SerializedName("attribute_group_id")
    @Expose
    private String attributeGroupId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attribute")
    @Expose
    private ArrayList<Attribute> attribute = new ArrayList<Attribute>();

    /**
     * 
     * @return
     *     The attributeGroupId
     */
    public String getAttributeGroupId() {
        return attributeGroupId;
    }

    /**
     * 
     * @param attributeGroupId
     *     The attribute_group_id
     */
    public void setAttributeGroupId(String attributeGroupId) {
        this.attributeGroupId = attributeGroupId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The attribute
     */
    public ArrayList<Attribute> getAttribute() {
        return attribute;
    }

    /**
     * 
     * @param attribute
     *     The attribute
     */
    public void setAttribute(ArrayList<Attribute> attribute) {
        this.attribute = attribute;
    }

}
