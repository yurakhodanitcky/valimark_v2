
package ua.com.valimarkbiz.models.getcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCart {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private CartProducts cartProducts;

    /**
     * 
     * @return
     *     The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The data
     */
    public CartProducts getCartProducts() {
        return cartProducts;
    }

    /**
     * 
     * @param cartProducts
     *     The data
     */
    public void setCartProducts(CartProducts cartProducts) {
        this.cartProducts = cartProducts;
    }

}
