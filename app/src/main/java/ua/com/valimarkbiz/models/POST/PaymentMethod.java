package ua.com.valimarkbiz.models.POST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yurak on 19.06.2016.
 */
public class PaymentMethod {

    @SerializedName("payment_method")
    @Expose
    private final String paymentMethod = "cod";
    @SerializedName("agree")
    @Expose
    private final String agree = "1";
    @SerializedName("comment")
    @Expose
    private final String comment = "";

    /**
     * @return The paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @return The agree
     */
    public String getAgree() {
        return agree;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }
}