
package ua.com.valimarkbiz.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;


public class Data extends SugarRecord
{

    @SerializedName("session")
    @Expose
    private String session;

   /* public Data(){

    }*/

    public Data(String session) {
        this.session = session;
    }

    /**
     * 
     * @return
     *     The session
     */
    public String getSession() {
        return session;
    }

    /**
     * 
     * @param session
     *     The session
     */
    public void setSession(String session) {
        this.session = session;
    }

}
