package ua.com.valimarkbiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.orm.SugarContext;

import ua.com.valimarkbiz.api.ApiFactory;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.ConnectionDetector;
import ua.com.valimarkbiz.helper.PrefUtils;
import ua.com.valimarkbiz.loaders.SessionLoader;
import ua.com.valimarkbiz.models.Data;
import ua.com.valimarkbiz.models.SessionRequest;

/**
 * Created by yurak on 18.05.2016.
 */
public class Splash extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Response>{

   /* public static final String APP_PREFERENCES = "vbSsettings";
    public static final String APP_PREFERENCES_TOKEN = "token";
    private SharedPreferences mSettings;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        //mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);


        SugarContext.init(this);
        ApiFactory.init();
        ConnectionDetector detector = new ConnectionDetector(this);

        if (detector.isConnected()) {
           /* if(PrefUtils.isValueExist(this, PrefUtils.APP_PREFERENCES_TOKEN)){
                this.startActivity(new Intent(this, MainActivity.class));
            }else {*/
                getSupportLoaderManager().initLoader(0, Bundle.EMPTY, this);
            //}
        }
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {

           return new SessionLoader(Splash.this);
        }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        if (loader.getId() == 0) {
            SessionRequest session = data.getTypedAnswer();
            Data data1 = session.getData();
            // Create a RealmConfiguration which is to locate Realm file in package's "files" directory.
            Data.deleteAll(Data.class);

            Log.d("SPLASH", data1.getSession());
            data1.save();
            PrefUtils.SESSION =  data1.getSession();
            //PrefUtils.saveToPrefs(this, PrefUtils.APP_PREFERENCES_TOKEN, data1.getSession());
            //SharedPreferences.Editor editor = mSettings.edit();
            //editor.putString(APP_PREFERENCES_TOKEN, data1.getSession());
            //editor.apply();
            Toast.makeText(this, session.getData().getSession(), Toast.LENGTH_SHORT).show();
            this.startActivity(new Intent(this, MainActivity.class));
        }
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {

    }
}
