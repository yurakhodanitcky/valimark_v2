package ua.com.valimarkbiz.api;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.com.valimarkbiz.api.interfaces.AccountInfoService;
import ua.com.valimarkbiz.api.interfaces.CartAddService;
import ua.com.valimarkbiz.api.interfaces.CartGetService;
import ua.com.valimarkbiz.api.interfaces.CategoriesService;
import ua.com.valimarkbiz.api.interfaces.GestPaymantService;
import ua.com.valimarkbiz.api.interfaces.GestShipingService;
import ua.com.valimarkbiz.api.interfaces.GetPaymentService;
import ua.com.valimarkbiz.api.interfaces.GetShipingService;
import ua.com.valimarkbiz.api.interfaces.LimitProductService;
import ua.com.valimarkbiz.api.interfaces.LoginService;
import ua.com.valimarkbiz.api.interfaces.PostConfirm;
import ua.com.valimarkbiz.api.interfaces.PostPaymentService;
import ua.com.valimarkbiz.api.interfaces.PostShipingService;
import ua.com.valimarkbiz.api.interfaces.ProductListService;
import ua.com.valimarkbiz.api.interfaces.PutConfirm;
import ua.com.valimarkbiz.api.interfaces.SessionService;
import ua.com.valimarkbiz.api.interfaces.SingleProductService;
import ua.com.valimarkbiz.cookies.AddCookiesInterceptor;
import ua.com.valimarkbiz.cookies.ReceivedCookiesInterceptor;
import ua.com.valimarkbiz.models.Data;

public class ApiFactory {


    private static final Gson GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    public static OkHttpClient client;// = new OkHttpClient();
    private static OkHttpClient.Builder builder;// = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor interceptor;// = new HttpLoggingInterceptor();
    private static CookieJar cookieJar;


    public static void init(/*Context context*/) {
        cookieJar = new CookieJar() {
            private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

            @Override
            public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                cookieStore.put(url.host(), cookies);
                for (Cookie c:cookies)
                {
                    Log.d("COOCIE", url.host() + " ///  " + c.toString());
                }
            }

            @Override
            public List<Cookie> loadForRequest(HttpUrl url) {
                List<Cookie> cookies = cookieStore.get(url.host());
               /* if (cookies != null) {
                    for (Cookie c : cookies) {
                        Log.d("COOCIE", url.host() + " ///  " + c.toString());
                    }
                }*/
                return cookies != null ? cookies : new ArrayList<Cookie>();
            }
        };
        interceptor = new HttpLoggingInterceptor();
        builder = new OkHttpClient.Builder();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //builder.addInterceptor(new AddCookiesInterceptor(context));
        //builder.addInterceptor(new ReceivedCookiesInterceptor(context));
        builder.addInterceptor(interceptor);
        builder.cookieJar(cookieJar);
        client = builder.build();
    }


    @NonNull
    public static GetShipingService getShipingService() {
        return getRetrofit().create(GetShipingService.class);
    }

    @NonNull
    public static AccountInfoService getAccountInfoService() {
        return getRetrofit().create(AccountInfoService.class);
    }

    @NonNull
    public static LoginService getLoginService() {
        return getRetrofit().create(LoginService.class);
    }

    @NonNull
    public static SessionService getSessionService() {
        return getRetrofit().create(SessionService.class);
    }

    @NonNull
    public static CategoriesService getCategoriesService() {
        return getRetrofit().create(CategoriesService.class);
    }

    @NonNull
    public static ProductListService getProductsListService() {
        return getRetrofit().create(ProductListService.class);
    }

    @NonNull
    public static LimitProductService getLimitProductService() {
        return getRetrofit().create(LimitProductService.class);
    }

    @NonNull
    public static SingleProductService getSingleProductService() {
        return getRetrofit().create(SingleProductService.class);
    }

    @NonNull
    public static CartAddService getCartAddService() {
        return getRetrofit().create(CartAddService.class);
    }

    @NonNull
    public static CartGetService getCartService() {
        return getRetrofit().create(CartGetService.class);
    }

    @NonNull
    public static GestPaymantService getGestPaymantService() {
        return getRetrofit().create(GestPaymantService.class);
    }

    @NonNull
    public static GestShipingService getGestShipingService() {
        return getRetrofit().create(GestShipingService.class);
    }

    @NonNull
    public static PostShipingService postShipingService() {
        return getRetrofit().create(PostShipingService.class);
    }

    @NonNull
    public static GetPaymentService getPaymentService() {
        return getRetrofit().create(GetPaymentService.class);
    }

    @NonNull
    public static PostPaymentService postPaymentService() {
        return getRetrofit().create(PostPaymentService.class);
    }

    @NonNull
    public static PostConfirm postConfirm() {
        return getRetrofit().create(PostConfirm.class);
    }

    @NonNull
    public static PutConfirm putConfirm() {
        return getRetrofit().create(PutConfirm.class);
    }

    @NonNull
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://701822.valimsit.web.hosting-test.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }
}