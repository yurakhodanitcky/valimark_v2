package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.com.valimarkbiz.models.POST.Gest;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public interface GestPaymantService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @POST("index.php?route=rest/guest/guest")
    Call<Sucsess> newPost(@Header("X-Oc-Session") String session, @Body Gest gest);
}
