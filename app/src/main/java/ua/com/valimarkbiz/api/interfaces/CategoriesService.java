package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import ua.com.valimarkbiz.models.Categories;

public interface CategoriesService
{
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
	@GET("index.php?route=feed/rest_api/categories")
    Call<Categories> categories(@Header("X-Oc-Session") String session);
}
