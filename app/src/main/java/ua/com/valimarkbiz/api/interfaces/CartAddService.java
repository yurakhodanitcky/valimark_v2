package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.com.valimarkbiz.models.POST.CartAdd;
import ua.com.valimarkbiz.models.POST.CartAddSucsess;

/**
 * Created by yurak on 10.06.2016.
 */
public interface CartAddService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @POST("index.php?route=rest/cart/cart")
    Call<CartAddSucsess> cartAdd (@Header("X-Oc-Session") String session, @Body CartAdd cartAdd);
}

