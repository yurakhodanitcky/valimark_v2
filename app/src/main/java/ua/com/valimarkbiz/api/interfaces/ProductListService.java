package ua.com.valimarkbiz.api.interfaces;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import ua.com.valimarkbiz.models.product.ProductResponse;

public interface ProductListService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @GET("index.php?route=feed/rest_api/products")
    Call<ProductResponse> productResponce (@Header("X-Oc-Session") String session, @Query("category") String category);
}
