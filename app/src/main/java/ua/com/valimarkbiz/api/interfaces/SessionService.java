package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import ua.com.valimarkbiz.models.SessionRequest;


public interface SessionService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @GET("index.php?route=feed/rest_api/session")
    Call<SessionRequest> session();
}
