package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public interface GetPaymentService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @GET("index.php?route=rest/payment_method/payments")
    Call<Sucsess> getPayment (@Header("X-Oc-Session") String session);
}
