package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.com.valimarkbiz.models.LoginClass;
import ua.com.valimarkbiz.models.LoginRecponce;

/**
 * Created by yurak on 01.06.2016.
 */
public interface LoginService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @POST("index.php?route=rest/login/login")
    Call<LoginRecponce> login(@Header("X-Oc-Session") String session, @Body LoginClass user);
}
