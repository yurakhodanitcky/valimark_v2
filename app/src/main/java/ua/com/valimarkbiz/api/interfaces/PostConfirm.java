package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public interface PostConfirm {

    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @POST("index.php?route=rest/confirm/confirm")
    Call<Sucsess> postConfirm(@Header("X-Oc-Session") String session);
}
