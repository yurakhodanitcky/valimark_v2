package ua.com.valimarkbiz.api.interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.com.valimarkbiz.models.POST.ShippingMethod;
import ua.com.valimarkbiz.models.POST.Sucsess;

/**
 * Created by yurak on 19.06.2016.
 */
public interface PostShipingService {
    @Headers({"X-Oc-Merchant-Id: 123",
            "Accept: application/json",
            "Content-Type: application/json"})
    @POST("index.php?route=rest/shipping_method/shippingmethods")
    Call<Sucsess> call(@Header("X-Oc-Session") String session, @Body ShippingMethod shippingMethod);
}
