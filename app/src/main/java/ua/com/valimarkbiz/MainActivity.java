package ua.com.valimarkbiz;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import ua.com.valimarkbiz.adapters.CategoriesAdapter;
import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.helper.ConnectionDetector;
import ua.com.valimarkbiz.loaders.AccountLoader;
import ua.com.valimarkbiz.loaders.CategoryLoader;
import ua.com.valimarkbiz.loaders.LoginLoader;
import ua.com.valimarkbiz.loaders.GetShipinMethodsLoader;
import ua.com.valimarkbiz.models.Categories;
import ua.com.valimarkbiz.models.product.ProductResponse;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LoaderManager.LoaderCallbacks<Response> {

    private CategoriesAdapter adapter;

    private ArrayList<ProductResponse> productResponses = new ArrayList<>();

    Toolbar toolbar;
    NavigationView navigationView;
    RecyclerView recyclerView;
    DrawerLayout drawer;

    @Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		navigationView = (NavigationView)findViewById(R.id.nav_view);
		recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
		drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        ConnectionDetector detector = new ConnectionDetector(this);

        if (detector.isConnected()) {
                getSupportLoaderManager().initLoader(1, Bundle.EMPTY, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            getSupportLoaderManager().initLoader(22, Bundle.EMPTY, this);
        } else if (id == R.id.nav_gallery) {
            getSupportLoaderManager().initLoader(25, Bundle.EMPTY, this);

        } else if (id == R.id.nav_slideshow) {
            getSupportLoaderManager().initLoader(26, Bundle.EMPTY, this);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        Loader<Response> mLoader = null;
        if (id == 1) {
            mLoader = new CategoryLoader(MainActivity.this);
        }
        else if(id == 22){
            mLoader = new LoginLoader(MainActivity.this);
        }
        else if(id == 25){
            mLoader = new AccountLoader(MainActivity.this);
        } else if(id == 26){
            mLoader = new GetShipinMethodsLoader(MainActivity.this);
        }
        return mLoader;
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
     if (loader.getId() == 1) {

            Categories categories = data.getTypedAnswer();

            ArrayList<Categories.Category> cattt = categories.getCategories();
            adapter = new CategoriesAdapter(this, cattt);
            recyclerView.setAdapter(adapter);
        }
        else if(loader.getId() == 22){
        /* LoginRecponce loginRecponce = data.getTypedAnswer();
         Log.d("LOGIN", loginRecponce.getSuccess().toString());*/
     }
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {

    }
}
