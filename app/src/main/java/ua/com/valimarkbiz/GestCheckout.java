package ua.com.valimarkbiz;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;

import ua.com.valimarkbiz.api.responce.Response;
import ua.com.valimarkbiz.loaders.ConfirmLoader;
import ua.com.valimarkbiz.loaders.GestPaymentLoader;
import ua.com.valimarkbiz.loaders.GetPaymentMethodsLoader;
import ua.com.valimarkbiz.loaders.GetShipinMethodsLoader;
import ua.com.valimarkbiz.loaders.PostPaymentMethodLoader;
import ua.com.valimarkbiz.loaders.PostShipingMethodLoader;
import ua.com.valimarkbiz.models.POST.Gest;

/**
 * Created by yurak on 18.06.2016.
 */
public class GestCheckout extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Response>, View.OnClickListener {

    private String zoneId;
    private Button gest;
    private Button getShiping;
    private Button confirm;
    private Button getPayment;
    private Button postShiping;
    private Button postPayment;
    private EditText mName;
    private EditText mLastName;
    private EditText mSity;
    private EditText mDelivery;
    private EditText mEmail;
    private EditText mPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gest_checkout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_g);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle(R.string.checkout);
        Spinner spinner = (Spinner) findViewById(R.id.regions);

        final String[] keys = this.getResources().getStringArray(R.array.regions);
        String[] values = this.getResources().getStringArray(R.array.regions_id);
        final HashMap<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < Math.min(keys.length, values.length); ++i) {
            map.put(keys[i], values[i]);
        }

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.regions, R.layout.spin_l);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                zoneId = map.get(keys[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mName = (EditText) findViewById(R.id.input_name);
        mLastName = (EditText) findViewById(R.id.input_last_name);
        mSity = (EditText) findViewById(R.id.inddput_sity);
        mDelivery = (EditText) findViewById(R.id.ivnput_delivery);
        mEmail = (EditText) findViewById(R.id.input_email);
        mPhone = (EditText) findViewById(R.id.input_phone);

        gest = (Button) findViewById(R.id.button_confirm);
        getShiping = (Button) findViewById(R.id.getShiping);
        postShiping = (Button) findViewById(R.id.postShiping);
        getPayment = (Button) findViewById(R.id.getPayment);
        postPayment = (Button) findViewById(R.id.postPayment);
        confirm = (Button) findViewById(R.id.confirm);

        gest.setOnClickListener(this);
        getShiping.setOnClickListener(this);
        postShiping.setOnClickListener(this);
        getPayment.setOnClickListener(this);
        postPayment.setOnClickListener(this);
        confirm.setOnClickListener(this);



        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gest gest = new Gest(mDelivery.getText().toString(), mSity.getText().toString(), mEmail.getText().toString()
                ,mName.getText().toString(), mLastName.getText().toString(), mPhone.getText().toString(), zoneId);
                Bundle bundle = new Bundle();
                bundle.putSerializable("GEST", gest);
                getSupportLoaderManager().initLoader(0, bundle, GestCheckout.this);
            }
        });*/
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
       Loader<Response> mLoader = null;
        if(id == 0) {
            Gest g = (Gest) args.getSerializable("GEST");
            mLoader = new GestPaymentLoader(this, g);
        }else if(id == 1){
            mLoader = new GetShipinMethodsLoader(this);
        }else if(id == 2){
            mLoader = new PostShipingMethodLoader(this);
        }else if (id == 3){
            mLoader = new GetPaymentMethodsLoader(this);
        }else if (id == 4){
            mLoader = new PostPaymentMethodLoader(this);
        }else if (id == 5){
            mLoader = new ConfirmLoader(this);
        }
        return mLoader;
    }


    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        if (loader.getId() == 0) {
            getSupportLoaderManager().initLoader(1, Bundle.EMPTY, this);
        } else if (loader.getId() == 1) {
            getSupportLoaderManager().initLoader(2, Bundle.EMPTY, this);
        } else if (loader.getId() == 2) {
            getSupportLoaderManager().initLoader(3, Bundle.EMPTY, this);
        } else if (loader.getId() == 3) {
            getSupportLoaderManager().initLoader(4, Bundle.EMPTY, this);
        } else if (loader.getId() == 4) {
            getSupportLoaderManager().initLoader(5, Bundle.EMPTY, this);
        } else if (loader.getId() == 5) {
            Toast.makeText(this, "FINISH", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_confirm:
                Gest gest = new Gest(mDelivery.getText().toString(), mSity.getText().toString(), mEmail.getText().toString()
                        ,mName.getText().toString(), mLastName.getText().toString(), mPhone.getText().toString(), zoneId);
                Bundle bundle = new Bundle();
                bundle.putSerializable("GEST", gest);
                getSupportLoaderManager().initLoader(0, bundle, GestCheckout.this);
                break;
           /* case R.id.getShiping:
                getSupportLoaderManager().initLoader(1, Bundle.EMPTY, this);
                break;
            case R.id.postShiping:
                getSupportLoaderManager().initLoader(2, Bundle.EMPTY, this);
                break;
            case R.id.getPayment:
                getSupportLoaderManager().initLoader(3, Bundle.EMPTY, this);
                break;
            case R.id.postPayment:
                getSupportLoaderManager().initLoader(4, Bundle.EMPTY, this);
                break;
            case R.id.confirm:
                getSupportLoaderManager().initLoader(5, Bundle.EMPTY, this);
                break;*/
        }

    }
}
